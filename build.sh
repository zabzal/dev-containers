#!/bin/sh

TEMPLATES="templates"
TEMPLATE=$1
REPO_NAME="z2a"
IMAGE_NAME=$TEMPLATE
TAG=$2


echo "Building image"
echo "Command: docker build -t $REPO_NAME/$IMAGE_NAME:$TAG $TEMPLATES/$TEMPLATE"

docker build -t $REPO_NAME/$IMAGE_NAME:$TAG $TEMPLATES/$TEMPLATE